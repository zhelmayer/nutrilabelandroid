package hu.logicloud.nutrilabel.bll;

public enum MOVEMENT_CODE {

    LABELING(20),
    ADD_TO_STOCK(70),
    REMOVE_FROM_STOCK(75);

    private final int value;

    MOVEMENT_CODE(final int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }
}
