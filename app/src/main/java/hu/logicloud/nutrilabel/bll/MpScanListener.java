package hu.logicloud.nutrilabel.bll;
import com.datalogic.decode.ReadListener;
import com.honeywell.aidc.BarcodeReader;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.Scanner;

public interface MpScanListener extends
        BarcodeReader.BarcodeListener,
        ReadListener,
        Scanner.DataListener,
        com.symbol.emdk.barcode.Scanner.StatusListener,
        EMDKManager.EMDKListener
{

}