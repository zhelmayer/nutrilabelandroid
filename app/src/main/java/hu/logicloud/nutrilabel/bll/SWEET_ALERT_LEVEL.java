package hu.logicloud.nutrilabel.bll;

public enum SWEET_ALERT_LEVEL {

    GOOD(0),
    ERROR(1),
    INFO(2);

    private final int value;

    SWEET_ALERT_LEVEL(final int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }
}
