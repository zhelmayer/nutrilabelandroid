package hu.logicloud.nutrilabel.common;

import android.os.StrictMode;

public class GlobalFunctions {

    GlobalFunctions(){}

    public static void enableStrictMode() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public static boolean isEanCode(String code){

        boolean isEAN = code
                .replace("|","/")
                .matches("[0-9]+") && code.length() == 13;

        return isEAN;
    }

    public static boolean isLabelCode(String code){
        boolean isLabelCode = code
                .replace("|", "/")
                .matches("\\$[^\\$]*+\\$");


        return isLabelCode;
    }

}
