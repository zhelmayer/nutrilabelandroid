package hu.logicloud.nutrilabel.common;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

public class GlobalVariables {

    private GlobalVariables(){}

    public static final String APP_LOG_TAG = "NUTRICIA";

    public static boolean APP_CRASH_REPORT_ENABLED = true;
    public static boolean APP_VIBRATION_ENABLED = true;
    public static boolean APP_BAD_READ_SOUND_ENABLED = true;
    public static float APP_BAD_READ_SOUND_VOLUME = 250f;
    public static String APP_RESOURCE_SERVER_ADDRESS = "nutrilabel.hu";
    //public static String APP_RESOURCE_SERVER_ADDRESS = "192.168.4.100";
    public static String APP_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ssZ";
    public static String APP_SECRET_KEY = "Bar12345Bar12345";

    public static final String APP_RESOURCE_URL = "https://" + APP_RESOURCE_SERVER_ADDRESS;
    public static final String APP_RESOURCE_APP_NAME = "";
    //public static final String APP_RESOURCE_APP_NAME = "labelcatalog";


    public static final String NUTRI_LABEL_CRASH_REPORT_ENABLED = "nutri_label_crash_report_enabled";
    public static final String NUTRI_LABEL_VIBRATION_ENABLED = "nutri_label_vibration_enabled";
    public static final String NUTRI_LABEL_BAD_READ_SOUND_ENABLED = "nutri_label_bad_read_sound_enabled";
    public static final String NUTRI_LABEL_BAD_READ_SOUND_VOLUME = "nutri_label_bad_read_sound_volume";
    public static final String NUTRI_LABEL_RESOURCE_SERVER_ADDRESS = "nutri_label_resource_server_address";
    public static final String NUTRI_LABEL_DATETIME_FORMAT = "nutri_label_datetime_format";

    public static final Map<String, Object> defaultConfigMap;
    static
    {
        defaultConfigMap = new HashMap<>();
        defaultConfigMap.put(NUTRI_LABEL_CRASH_REPORT_ENABLED, APP_CRASH_REPORT_ENABLED);
        defaultConfigMap.put(NUTRI_LABEL_VIBRATION_ENABLED, APP_VIBRATION_ENABLED);
        defaultConfigMap.put(NUTRI_LABEL_BAD_READ_SOUND_ENABLED, APP_BAD_READ_SOUND_ENABLED);
        defaultConfigMap.put(NUTRI_LABEL_BAD_READ_SOUND_VOLUME, APP_BAD_READ_SOUND_VOLUME);
        defaultConfigMap.put(NUTRI_LABEL_RESOURCE_SERVER_ADDRESS, APP_RESOURCE_SERVER_ADDRESS);
        defaultConfigMap.put(NUTRI_LABEL_DATETIME_FORMAT, APP_DATETIME_FORMAT);
    }

    @Setter
    @Getter
    private static String token;

    @Setter
    @Getter
    private static Integer warehouse;

    public static void setDefaultConfigs(FirebaseRemoteConfig firebaseRemoteConfig){
        APP_CRASH_REPORT_ENABLED = firebaseRemoteConfig.getBoolean(NUTRI_LABEL_CRASH_REPORT_ENABLED);
        APP_VIBRATION_ENABLED = firebaseRemoteConfig.getBoolean(NUTRI_LABEL_VIBRATION_ENABLED);
        APP_BAD_READ_SOUND_ENABLED = firebaseRemoteConfig.getBoolean(NUTRI_LABEL_BAD_READ_SOUND_ENABLED);
        APP_BAD_READ_SOUND_VOLUME = Float.valueOf(firebaseRemoteConfig.getString(NUTRI_LABEL_BAD_READ_SOUND_VOLUME));
        APP_RESOURCE_SERVER_ADDRESS = firebaseRemoteConfig.getString(NUTRI_LABEL_RESOURCE_SERVER_ADDRESS);
        APP_DATETIME_FORMAT = firebaseRemoteConfig.getString(NUTRI_LABEL_DATETIME_FORMAT);
    }
}
