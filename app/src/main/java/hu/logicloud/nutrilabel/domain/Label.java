
package hu.logicloud.nutrilabel.domain;

import java.io.Serializable;

import lombok.Data;

@Data
public class Label implements Serializable {
    private Integer id;
    private Integer product_id;
    private String description;
    private String label_code;
    private String country;
    private String status;
    private String label_date_from;
    private String label_date_to;
    private String label_front;
    private String label_back;
    private String label_mid;
    private String label_pdf;
    private String eancode;
    private Integer sapcode;
    private Integer labelunit;
    private String ean_ntbc;
    private String comment;
    private String createdat;
    private String updatedat;
    private String prodcat;
    private String checkall;
}
