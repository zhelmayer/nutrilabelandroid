package hu.logicloud.nutrilabel.domain;

import lombok.Data;

@Data
public class Login {
    private String status;
    private String token;
    private String WH;
}
