package hu.logicloud.nutrilabel.domain;

import lombok.Data;

@Data
public class QrCode {
    private String name;
    private String code;
    private String warehouse;
}
