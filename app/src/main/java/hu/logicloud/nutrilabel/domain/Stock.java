package hu.logicloud.nutrilabel.domain;

import lombok.Data;

@Data
public class Stock {
    private String id;
    private String qty;
    private String minqty;
    private String createdat;
    private String updatedat;
    private Warehouse warehouse;
    private Label label;
}
