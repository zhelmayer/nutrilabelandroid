package hu.logicloud.nutrilabel.domain;

import lombok.Data;

@Data
public class Warehouse {
    private String id;
    private String name;
    private String code;
    private String location;
    private String createdate;
}
