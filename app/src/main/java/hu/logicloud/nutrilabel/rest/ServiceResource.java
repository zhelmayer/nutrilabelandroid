package hu.logicloud.nutrilabel.rest;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import hu.logicloud.nutrilabel.common.GlobalVariables;

import hu.logicloud.nutrilabel.domain.Label;
import hu.logicloud.nutrilabel.domain.Login;
import hu.logicloud.nutrilabel.domain.Stock;

public class ServiceResource {

    private ServiceResource(){}

    public static List<Label> getLabelByEanCodeOrLabelCode(String token, String isEAN, String code, Integer warehouseId){

        Services labelClient = ServiceGenerator.createService(Services.class, token);
        List<Label> label = new ArrayList<>();

        try {
            label = labelClient.getLabelByEanCodeOrLabelCode(isEAN, code, warehouseId, 14241).execute().body();
        } catch (Exception e) {
            Log.e(GlobalVariables.APP_LOG_TAG, e.getLocalizedMessage(), e);
        }

        return label;
    }

    public static List<List<Stock>> getStockByEanCodeOrLabelCodeAndWarehouse(String token, String isEAN, String code, Integer warehouseId){

        Services labelClient = ServiceGenerator.createService(Services.class, token);
        List<List<Stock>> label = new ArrayList<>();

        try {
            label = labelClient.getStockByEanCodeOrLabelCodeAndWarehouse(isEAN, code, warehouseId, 14241).execute().body();
        } catch (Exception e) {
            Log.e(GlobalVariables.APP_LOG_TAG, e.getLocalizedMessage(), e);
        }

        return label;
    }

    public static Boolean setStockByLabelCodeAndLabelCodeAndWarehouse(String token, String labelCode, Integer moveCode, Integer qty, String lotNo, Integer warehouseId){

        Services client = ServiceGenerator.createService(Services.class, token);
        Boolean status = false;

        try {
            status = client.setStockByLabelCodeAndMoveCodeAndQtyAndLotNoAndWarehouseId(labelCode, moveCode, qty, lotNo, warehouseId, 14241).execute().body();
        } catch (Exception e) {
            Log.e(GlobalVariables.APP_LOG_TAG, e.getLocalizedMessage(), e);
        }

        return status;
    }

    public static Login login(String code){

        Login login = new Login();

        Services client = ServiceGenerator.createService(Services.class, "");

        try {
            login = client.login(code, 14241).execute().body();
        } catch (Exception e) {
            Log.e(GlobalVariables.APP_LOG_TAG, e.getLocalizedMessage(), e);
        }

        return login;
    }
}

