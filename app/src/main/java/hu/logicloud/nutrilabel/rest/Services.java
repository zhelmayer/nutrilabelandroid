package hu.logicloud.nutrilabel.rest;

import java.util.List;

import hu.logicloud.nutrilabel.common.GlobalVariables;
import hu.logicloud.nutrilabel.domain.Label;
import hu.logicloud.nutrilabel.domain.Login;
import hu.logicloud.nutrilabel.domain.Stock;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Services {

    @GET("/" + GlobalVariables.APP_RESOURCE_APP_NAME + "hu/get_label")
    Call<List<Label>> getLabelByEanCodeOrLabelCode(
            @Query("isEAN") String isEAN,
            @Query("code") String code,
            @Query("WH") Integer warehouseId,
            @Query("XDEBUG_SESSION_START") Integer debug
    );

    @GET("/" + GlobalVariables.APP_RESOURCE_APP_NAME + "hu/get_stock")
    Call<List<List<Stock>>> getStockByEanCodeOrLabelCodeAndWarehouse(
            @Query("isEAN") String isEAN,
            @Query("code") String code,
            @Query("WH") Integer warehouseId,
            @Query("XDEBUG_SESSION_START") Integer debug
    );

    @FormUrlEncoded
    @POST("/" + GlobalVariables.APP_RESOURCE_APP_NAME + "hu/set_stock")
    Call<Boolean> setStockByLabelCodeAndMoveCodeAndQtyAndLotNoAndWarehouseId(
            @Field("label") String labelCode,
            @Field("movecode") Integer moveCode,
            @Field("qty") Integer qty,
            @Field("lot") String lot,
            @Field("WH") Integer warehouseId,
            @Field("XDEBUG_SESSION_START") Integer debug
    );

    @FormUrlEncoded
    @POST("/" + GlobalVariables.APP_RESOURCE_APP_NAME + "hu/api_login")
    Call<Login> login(
            @Field("code") String code,
            @Query("XDEBUG_SESSION_START") Integer debug
    );
}

