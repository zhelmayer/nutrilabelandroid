package hu.logicloud.nutrilabel.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.datalogic.decode.BarcodeManager;
import com.datalogic.decode.DecodeResult;
import com.honeywell.aidc.BarcodeFailureEvent;
import com.honeywell.aidc.BarcodeReadEvent;
import com.honeywell.aidc.BarcodeReader;
import com.honeywell.aidc.ScannerUnavailableException;
import com.honeywell.aidc.UnsupportedPropertyException;

import com.jaredrummler.android.device.DeviceName;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;


import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hu.logicloud.nutrilabel.R;
import hu.logicloud.nutrilabel.bll.MpScanListener;
import hu.logicloud.nutrilabel.bll.SWEET_ALERT_LEVEL;
import hu.logicloud.nutrilabel.common.GlobalFunctions;
import hu.logicloud.nutrilabel.common.GlobalVariables;
import lombok.Getter;
import lombok.Setter;

public abstract class BaseActivity extends Activity implements
        MpScanListener,
        InternetConnectivityListener {

    @Getter
    @Setter
    private InternetAvailabilityChecker internetAvailabilityChecker;


    private com.honeywell.aidc.BarcodeReader honeywellReader;
    private com.honeywell.aidc.AidcManager honeywellManager;

    private com.datalogic.decode.BarcodeManager datalogicBarcodeManager;


    private EMDKManager emdkManager = null;

    private com.symbol.emdk.barcode.BarcodeManager zebraBarcodeManager = null;

    private com.symbol.emdk.barcode.Scanner zebraScanner = null;

    int dataLength = 0;

    private boolean isScannerActive;



    @Getter
    private static Vibrator vibrator;

    private String barcodeData = "";

    @Getter
    @Setter
    private String characterSet;

    @Getter
    @Setter
    private String codeId;

    @Getter
    @Setter
    private String aimId;

    @Getter
    @Setter
    private String timestamp;

    @Getter
    @Setter
    private boolean isConnectedToInternet = true;

    private SweetAlertDialog networkSweetDialog;

    private static SweetAlertDialog commonSweetDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSweetDialog();
        setInternetAvailabilityChecker(InternetAvailabilityChecker.getInstance());
        getInternetAvailabilityChecker().addInternetConnectivityListener(this);

        GlobalFunctions.enableStrictMode();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setupBarcodeReaders();
    }

    private void setSweetDialog() {
        networkSweetDialog = new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        networkSweetDialog.setCancelable(false);
        networkSweetDialog.setTitleText(getString(R.string.no_internet_error_title));
        networkSweetDialog.setContentText(getString(R.string.no_internet_error));
        networkSweetDialog.setCustomImage(R.drawable.no_connection);
        networkSweetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                networkSweetDialog.getButton(SweetAlertDialog.BUTTON_CONFIRM).setVisibility(View.GONE);
            }
        });
    }

    public static void showSweetAlertdialog(final Context context, final SWEET_ALERT_LEVEL level, String message, SweetAlertDialog.OnSweetClickListener callback){
        switch (level){
            case GOOD:
                commonSweetDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                break;
            case ERROR:{
                errorFeedback(context);
                commonSweetDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                break;
            }
            case INFO:
                commonSweetDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
                break;
        }

        commonSweetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button okButton = commonSweetDialog.getButton(SweetAlertDialog.BUTTON_CONFIRM);
                okButton.setText(R.string.sweet_alert_ok_btn);
                switch(level){
                    case GOOD:
                        okButton.setBackground(context.getResources().getDrawable(R.drawable.sweet_alert_bg_good));
                        break;
                    case ERROR:
                        okButton.setBackground(context.getResources().getDrawable(R.drawable.sweet_alert_bg_error));
                        break;
                    case INFO:
                        okButton.setBackground(context.getResources().getDrawable(R.drawable.sweet_alert_bg_info));
                        break;
                }
            }
        });

        commonSweetDialog.setCancelable(false);
        commonSweetDialog.setContentText(message);
        commonSweetDialog.setConfirmClickListener(callback);
        commonSweetDialog.show();
    }

    private void setupBarcodeReaders() {

        if(getDeviceName().equalsIgnoreCase("TC25")) setupZebra();
        if(getDeviceName().equalsIgnoreCase("DL-AXIST")) setupDatalogic();
        if(getDeviceName().equalsIgnoreCase("CX-AN075")) setupHoneywell();

        vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
    }

    private void setupHoneywell() {
        com.honeywell.aidc.AidcManager.create(BaseActivity.this, new com.honeywell.aidc.AidcManager.CreatedCallback()
        {
            @Override
            public void onCreated(com.honeywell.aidc.AidcManager aidcManager)
            {
                honeywellManager = aidcManager;
                honeywellReader = honeywellManager.createBarcodeReader();

                try {
                    honeywellReader.setProperty(BarcodeReader.PROPERTY_CODE_128_ENABLED, true);
                    honeywellReader.setProperty(BarcodeReader.PROPERTY_CODE_39_ENABLED, true);
                    honeywellReader.setProperty(BarcodeReader.PROPERTY_EAN_8_ENABLED, true);
                    honeywellReader.setProperty(BarcodeReader.PROPERTY_EAN_13_ENABLED, true);
                    honeywellReader.setProperty(BarcodeReader.PROPERTY_EAN_13_CHECK_DIGIT_TRANSMIT_ENABLED, true);
                    honeywellReader.setProperty(BarcodeReader.PROPERTY_QR_CODE_ENABLED, true);

                    honeywellReader.setProperty(BarcodeReader.PROPERTY_TRIGGER_CONTROL_MODE, BarcodeReader.TRIGGER_CONTROL_MODE_AUTO_CONTROL);
                } catch (UnsupportedPropertyException e) {
                    Log.e(GlobalVariables.APP_LOG_TAG, e.getLocalizedMessage(), e);
                }

                honeywellReader.addBarcodeListener(BaseActivity.this);

                try {
                    honeywellReader.claim();
                } catch (ScannerUnavailableException e) {
                    Log.e(GlobalVariables.APP_LOG_TAG, e.getLocalizedMessage(), e);
                }
            }
        });
    }

    private void setupDatalogic() {
        datalogicBarcodeManager = new BarcodeManager();
        datalogicBarcodeManager.addReadListener(this);
    }

    private void setupZebra() {
        EMDKResults results = EMDKManager.getEMDKManager(
                getApplicationContext(), this);

        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            Toast.makeText(this, "EMDKManager Request Failed", Toast.LENGTH_LONG).show();
        }

        this.isScannerActive = false;
    }

    private void initializeScanner() throws ScannerException {
        if (zebraScanner == null) {

            zebraBarcodeManager = (com.symbol.emdk.barcode.BarcodeManager) this.emdkManager
                    .getInstance(EMDKManager.FEATURE_TYPE.BARCODE);

            zebraScanner = zebraBarcodeManager.getDevice(com.symbol.emdk.barcode.BarcodeManager.DeviceIdentifier.DEFAULT);

            zebraScanner.addDataListener(this);
            zebraScanner.addStatusListener(this);
            zebraScanner.triggerType = com.symbol.emdk.barcode.Scanner.TriggerType.HARD;

            zebraScanner.enable();
            zebraScanner.read();
            this.isScannerActive = true;
        }
    }

    private void deInitializeScanner() throws ScannerException {
        if (zebraScanner != null) {

            try {
                if(zebraScanner.isReadPending()){
                    zebraScanner.cancelRead();
                }
                zebraScanner.disable();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                zebraScanner.removeDataListener(this);
                zebraScanner.removeStatusListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                zebraScanner.release();
            } catch (Exception e) {
                e.printStackTrace();
            }

            isScannerActive = false;
            zebraScanner = null;
        }
    }

    @Override
    public void onBarcodeEvent(final BarcodeReadEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                barcodeData = event.getBarcodeData();
                characterSet = event.getCharset().toString();
                codeId = event.getCodeId();
                aimId = event.getAimId();
                timestamp = event.getTimestamp();

                if(isConnectedToInternet && (commonSweetDialog == null || !commonSweetDialog.isShowing())){
                    onSuccessRead();
                }
            }
        });
    }

    public abstract void onSuccessRead();

    @Override
    public void onFailureEvent(BarcodeFailureEvent arg0) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                errorFeedback(BaseActivity.this);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        resumeHoneywell();
        resumeDatalogic();
        resumeZebra();
    }

    private void resumeZebra() {
        if(zebraScanner != null){
            zebraScanner.addDataListener(this);
            zebraScanner.addStatusListener(this);
        }
    }

    private void resumeDatalogic() {
        if (datalogicBarcodeManager != null) {
            datalogicBarcodeManager.addReadListener(this);
        }
    }

    private void resumeHoneywell() {
        if (honeywellReader != null) {
            try {
                honeywellReader.claim();
            } catch (ScannerUnavailableException e) {
                Log.e(GlobalVariables.APP_LOG_TAG, e.getLocalizedMessage(), e);
                Toast.makeText(BaseActivity.this, R.string.scannar_unavailable, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRead(DecodeResult decodeResult) {
        barcodeData = decodeResult.getText().trim();
        if(isConnectedToInternet && (commonSweetDialog == null || !commonSweetDialog.isShowing())){
            onSuccessRead();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        pauseHoneywell();
        pauseDatalogic();
        pauseZebra();
    }

    private void pauseZebra() {
        if (this.emdkManager != null) {
            this.emdkManager.release();
            this.emdkManager = null;
        }

        try {
            deInitializeScanner();
        } catch (ScannerException e) {
            e.printStackTrace();
        }
    }

    private void pauseDatalogic() {
        if (datalogicBarcodeManager != null) {
            datalogicBarcodeManager.release();
        }
    }

    private void pauseHoneywell() {
        if (honeywellReader != null) {
            honeywellReader.release();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        destroyHoneywell();
        destroyDatalogic();
        destroyZebra();

        if(internetAvailabilityChecker != null){
            internetAvailabilityChecker
                    .removeInternetConnectivityChangeListener(this);

        }
    }

    private void destroyZebra() {
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }

        try {
            deInitializeScanner();
        } catch (ScannerException e) {
            e.printStackTrace();
        }
    }

    private void destroyDatalogic() {
        if (datalogicBarcodeManager != null) {
            datalogicBarcodeManager.removeReadListener(this);
        }
    }

    private void destroyHoneywell() {
        if (honeywellReader != null) {
            honeywellReader.removeBarcodeListener(this);
        }
    }

    public String getDeviceName(){
        return DeviceName.getDeviceName();
    }

    public String getBarcodeData(){
        return barcodeData.replace("|", "/");
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        isConnectedToInternet = isConnected;
        if(!isConnected){
            networkSweetDialog.show();
        }else{
            networkSweetDialog.dismiss();
        }
    }

    public static void errorFeedback(Context context) {
        if(GlobalVariables.APP_VIBRATION_ENABLED){
            errorVibarteNotify();
        }
        if(GlobalVariables.APP_BAD_READ_SOUND_ENABLED){
            errorSoundNotify(context);
        }
    }

    private static void errorVibarteNotify()
    {
        vibrator.vibrate(500);
    }

    private static void errorSoundNotify(Context context)
    {
        final MediaPlayer mp = MediaPlayer.create(context, R.raw.system_fault);
        mp.setVolume(
                GlobalVariables.APP_BAD_READ_SOUND_VOLUME,
                GlobalVariables.APP_BAD_READ_SOUND_VOLUME
        );
        mp.start();
    }



    //-----------------------------
    @Override
    public void onClosed() {

        if (this.emdkManager != null) {
            this.emdkManager.release();
            this.emdkManager = null;
        }

        try {
            deInitializeScanner();
        } catch (ScannerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;

        try {
            initializeScanner();
        } catch (ScannerException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        new AsyncDataUpdate().execute(scanDataCollection);
    }

    @Override
    public void onStatus(StatusData statusData) {
        try {
            if (isScannerActive && !zebraScanner.isReadPending()) {
                zebraScanner.read();
            }

        } catch (ScannerException e) {
            e.printStackTrace();
        }
    }

    private class AsyncDataUpdate extends
            AsyncTask<ScanDataCollection, Void, String> {

        @Override
        protected String doInBackground(ScanDataCollection... params) {

            String statusStr = "";

            ScanDataCollection scanDataCollection = params[0];

            if (scanDataCollection != null &&
                    scanDataCollection.getResult() == ScannerResults.SUCCESS) {

                ArrayList<ScanDataCollection.ScanData> scanData = scanDataCollection
                        .getScanData();

                for (ScanDataCollection.ScanData data : scanData) {
                    barcodeData = data.getData();
                    ScanDataCollection.LabelType labelType = data.getLabelType();
                    statusStr = barcodeData + " " + labelType;
                }
            }

            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    if(isConnectedToInternet && (commonSweetDialog == null || !commonSweetDialog.isShowing())){
                        onSuccessRead();
                    }
                }
            });

            return statusStr;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


    //-----------------------------------

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (zebraScanner != null) {
                zebraScanner.removeDataListener(this);
                zebraScanner.removeStatusListener(this);
                zebraScanner.disable();
                zebraScanner = null;
            }
        } catch (ScannerException e) {
            e.printStackTrace();
        }

        try {
            deInitializeScanner();
        } catch (ScannerException e) {
            e.printStackTrace();
        }
    }
}
