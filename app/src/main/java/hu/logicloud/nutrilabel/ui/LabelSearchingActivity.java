package hu.logicloud.nutrilabel.ui;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;


import java.util.ArrayList;

import hu.logicloud.nutrilabel.R;
import hu.logicloud.nutrilabel.bll.SWEET_ALERT_LEVEL;
import hu.logicloud.nutrilabel.common.GlobalFunctions;
import hu.logicloud.nutrilabel.common.GlobalVariables;
import hu.logicloud.nutrilabel.domain.Label;
import hu.logicloud.nutrilabel.rest.ServiceResource;

public class LabelSearchingActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_label_searching);
        //getSupportActionBar().setTitle(R.string.label_searching_title);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onSuccessRead() {
        clearData();
        EditText editText = (EditText) findViewById(R.id.code);
        editText.setText(getBarcodeData());


        Boolean isEAN = GlobalFunctions.isEanCode(getBarcodeData());
        ArrayList<Label> labelList = (ArrayList<Label>) ServiceResource.getLabelByEanCodeOrLabelCode(
                GlobalVariables.getToken(),
                isEAN.toString(),
                getBarcodeData(),
                GlobalVariables.getWarehouse()
        );
        if(labelList != null && !labelList.isEmpty()){
            for(Label label : labelList){
                setData(label);
            }
        }else{
            BaseActivity.showSweetAlertdialog(this, SWEET_ALERT_LEVEL.ERROR, getString(R.string.label_is_not_found), null );
            editText.setText("");
        }
    }

    private void clearData() {
        TextView eanCode = (TextView) findViewById(R.id.eanCode);
        eanCode.setText("");

        TextView labelCode = (TextView) findViewById(R.id.labelCode);
        labelCode.setText("");

        TextView productName = (TextView) findViewById(R.id.productName);
        productName.setText("");
    }

    private void setData(Label l) {
        TextView eanCode = (TextView) findViewById(R.id.eanCode);
        eanCode.setText(l.getEancode());

        TextView labelCode = (TextView) findViewById(R.id.labelCode);
        labelCode.setText(l.getLabel_code());

        TextView productName = (TextView) findViewById(R.id.productName);
        productName.setText(l.getDescription());
    }
}
