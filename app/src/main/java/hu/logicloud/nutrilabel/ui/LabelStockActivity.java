package hu.logicloud.nutrilabel.ui;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import hu.logicloud.nutrilabel.R;
import hu.logicloud.nutrilabel.bll.SWEET_ALERT_LEVEL;
import hu.logicloud.nutrilabel.common.GlobalFunctions;
import hu.logicloud.nutrilabel.common.GlobalVariables;
import hu.logicloud.nutrilabel.domain.Stock;
import hu.logicloud.nutrilabel.rest.ServiceResource;

public class LabelStockActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_label_stock);
        //getSupportActionBar().setTitle(R.string.label_stock_title);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onSuccessRead() {
        clearData();
        EditText editText = (EditText) findViewById(R.id.code);
        editText.setText(getBarcodeData());

        Boolean isEAN = GlobalFunctions.isEanCode(getBarcodeData());
        ArrayList<List<Stock>> stock = (ArrayList<List<Stock>>) ServiceResource.getStockByEanCodeOrLabelCodeAndWarehouse(
                GlobalVariables.getToken(),
                isEAN.toString(),
                getBarcodeData(),
                GlobalVariables.getWarehouse()
        );

        if(stock != null && !stock.isEmpty()){
            for(List<Stock> list : stock){
                for(Stock s : list){
                    setData(s);
                }
            }
        }else{
            BaseActivity.showSweetAlertdialog(this, SWEET_ALERT_LEVEL.ERROR, getString(R.string.label_is_not_found), null );
            editText.setText("");
        }
    }

    private void clearData() {
        TextView eanCode = (TextView) findViewById(R.id.eanCode);
        eanCode.setText("");

        TextView labelCode = (TextView) findViewById(R.id.labelCode);
        labelCode.setText("");

        TextView qty = (TextView) findViewById(R.id.qty);
        qty.setText("");

        TextView warehouse = (TextView) findViewById(R.id.warehouse);
        warehouse.setText("");

        TextView productName = (TextView) findViewById(R.id.productName);
        productName.setText("");
    }

    private void setData(Stock s) {
        TextView eanCode = (TextView) findViewById(R.id.eanCode);
        eanCode.setText(s.getLabel().getEancode());

        TextView labelCode = (TextView) findViewById(R.id.labelCode);
        labelCode.setText(s.getLabel().getLabel_code());

        TextView qty = (TextView) findViewById(R.id.qty);
        qty.setText(s.getQty());

        TextView warehouse = (TextView) findViewById(R.id.warehouse);
        warehouse.setText(s.getWarehouse().getName());

        TextView productName = (TextView) findViewById(R.id.productName);
        productName.setText(s.getLabel().getDescription());
    }
}
