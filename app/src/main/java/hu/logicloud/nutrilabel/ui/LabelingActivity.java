package hu.logicloud.nutrilabel.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.transitionseverywhere.ChangeText;
import com.transitionseverywhere.TransitionManager;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hu.logicloud.nutrilabel.R;
import hu.logicloud.nutrilabel.bll.MOVEMENT_CODE;
import hu.logicloud.nutrilabel.bll.SWEET_ALERT_LEVEL;
import hu.logicloud.nutrilabel.common.GlobalFunctions;
import hu.logicloud.nutrilabel.common.GlobalVariables;
import hu.logicloud.nutrilabel.domain.Label;
import hu.logicloud.nutrilabel.rest.ServiceResource;

public class LabelingActivity extends BaseActivity {

    private Integer mCounter = 1;
    private Integer mLabelingQty = 5;
    private EditText mEanCode;
    private EditText mLabelCode;
    private String mLotNumber;
    private Label mLabel;
    private Boolean mReadAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_labeling);

        mLabel = (Label) getIntent().getSerializableExtra("label");
        mLabelingQty = getIntent().getIntExtra("labelingQty",0);
        mLotNumber = getIntent().getStringExtra("lotNumber");
        mReadAll = getIntent().getBooleanExtra("readAll", true);

        TextView mLabelingQtyText = (TextView) findViewById(R.id.labelingQty);
        mLabelingQtyText.setText(mLabelingQty.toString());

        mEanCode = (EditText) findViewById(R.id.eanCode);
        mLabelCode = (EditText) findViewById(R.id.labelCode);
    }

    @Override
    public void onSuccessRead() {
        boolean isEan = GlobalFunctions.isEanCode(getBarcodeData());
        setData(isEan);
    }

    private void setData(boolean isEAN) {

        if(isEAN){
            setControl(mEanCode, mLabelCode);
        }else{
            setControl(mLabelCode, mEanCode);
        }
    }

    private void setControl(EditText fireCode, EditText matchCode) {
        fireCode.setText(getBarcodeData());

        if(!TextUtils.isEmpty(matchCode.getText())){
            codePairing();
        }else{
            hideFeedback();
        }
    }

    private void codePairing() {
        boolean isPair = isPair();
        boolean isSuccessStockOperation = false;
        if(isPair){
            isSuccessStockOperation = setStock();
            if(isSuccessStockOperation){
                increaseCounter();
            }
        }
        showFeedback(isPair && isSuccessStockOperation);
        clearData();

        if(mCounter > mLabelingQty || (isPair && !mReadAll)){
            BaseActivity.showSweetAlertdialog(this, SWEET_ALERT_LEVEL.GOOD, getString(R.string.successfully_labeling), onConfirmBtnClick );
        }
    }

    private SweetAlertDialog.OnSweetClickListener onConfirmBtnClick = new SweetAlertDialog.OnSweetClickListener() {
        @Override
        public void onClick(SweetAlertDialog sweetAlertDialog) {
            sweetAlertDialog.dismiss();
            onBackPressed();
        }
    };

    private boolean setStock() {
        Boolean status = (Boolean) ServiceResource.setStockByLabelCodeAndLabelCodeAndWarehouse(
                GlobalVariables.getToken(),
                mLabelCode.getText().toString(),
                MOVEMENT_CODE.LABELING.getValue(),
                mReadAll ? 1 : mLabelingQty,
                mLotNumber,
                GlobalVariables.getWarehouse()
        );

        if(!status){
            Toast.makeText(this, R.string.unsuccessfully_stock_setting, Toast.LENGTH_SHORT).show();
        }

        return status;
    }

    private void increaseCounter() {
        mCounter++;
        if(mCounter <= mLabelingQty){
            ViewGroup transitionsContainer = (ViewGroup) findViewById(R.id.checkCounter);
            TransitionManager.beginDelayedTransition(transitionsContainer, new ChangeText().setChangeBehavior(ChangeText.CHANGE_BEHAVIOR_OUT_IN));
            TextView counter = (TextView)findViewById(R.id.counter);
            counter.setText(mCounter.toString());
        }
    }

    private void hideFeedback() {
        ViewGroup transitionsContainer = (ViewGroup) findViewById(R.id.pokaYoke);
        TransitionManager.beginDelayedTransition(transitionsContainer);
        TextView feedback = (TextView)findViewById(R.id.feedback);
        feedback.setVisibility(View.INVISIBLE);
    }

    private void showFeedback(boolean isOk) {
        if(!isOk) errorFeedback(this);

        TextView feedback = (TextView)findViewById(R.id.feedback);
        feedback.setTextColor(getResources().getColor(
                isOk ? R.color.colorGreen : R.color.colorRed
        ));
        feedback.setText(isOk ? getString(R.string.ok) : getString(R.string.error));
        feedback.setVisibility(View.VISIBLE);
    }

    private boolean isPair() {
        boolean isOkEanCode = mLabel.getEancode().equalsIgnoreCase(mEanCode.getText().toString());
        boolean isOkLabelCode = mLabel.getLabel_code().equalsIgnoreCase(mLabelCode.getText().toString());

        return isOkEanCode && isOkLabelCode;
    }

    private void clearData() {
        mEanCode.setText("");
        mLabelCode.setText("");
    }
}