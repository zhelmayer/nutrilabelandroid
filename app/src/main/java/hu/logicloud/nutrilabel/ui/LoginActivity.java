package hu.logicloud.nutrilabel.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;

import java.io.IOException;

import hu.logicloud.nutrilabel.R;
import hu.logicloud.nutrilabel.common.GlobalVariables;
import hu.logicloud.nutrilabel.domain.Login;
import hu.logicloud.nutrilabel.domain.QrCode;
import hu.logicloud.nutrilabel.rest.ServiceResource;
import io.fabric.sdk.android.Fabric;

public class LoginActivity extends BaseActivity {

    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setupInternetAvailabilityChecker();

        super.onCreate(savedInstanceState);

        setupCrashlytics();

        setContentView(R.layout.activity_login);

        // Initialize Firebase Remote Config.
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        // Define Firebase Remote Config Settings.
        FirebaseRemoteConfigSettings firebaseRemoteConfigSettings =
                new FirebaseRemoteConfigSettings.Builder()
                        .setDeveloperModeEnabled(true)
                        .build();

        // Define default config values. Defaults are used when fetched config values are not
        // available. Eg: if an error occurred fetching values from the server.

        // Apply config settings and default values.
        mFirebaseRemoteConfig.setConfigSettings(firebaseRemoteConfigSettings);
        mFirebaseRemoteConfig.setDefaults(GlobalVariables.defaultConfigMap);

        // Fetch remote config.
        fetchConfig();
    }

    public void fetchConfig() {
        long cacheExpiration = 3600; // 1 hour in seconds
        // If developer mode is enabled reduce cacheExpiration to 0 so that
        // each fetch goes to the server. This should not be used in release
        // builds.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings()
                .isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }
        mFirebaseRemoteConfig.fetch(cacheExpiration)

                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Make the fetched config available via
                        // FirebaseRemoteConfig get<type> calls.
                        mFirebaseRemoteConfig.activateFetched();
                        GlobalVariables.setDefaultConfigs(mFirebaseRemoteConfig);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // There has been an error fetching the config
                        Log.w(GlobalVariables.APP_LOG_TAG, "Error fetching config: " + e.getMessage());
                    }
                });
    }

    @Override
    public void onSuccessRead() {
        login(getBarcodeData());
    }

    private void login(String code) {
        Login mLogin = ServiceResource.login(code);
        if(mLogin != null && mLogin.getStatus() != null && mLogin.getStatus().equalsIgnoreCase("true")){
            GlobalVariables.setToken(mLogin.getToken());
            GlobalVariables.setWarehouse(Integer.valueOf(mLogin.getWH()));

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(this, R.string.invalid_login, Toast.LENGTH_SHORT).show();
        }
    }

    private void setupInternetAvailabilityChecker() {
        InternetAvailabilityChecker.init(this);
    }

    private void setupCrashlytics() {
        if(GlobalVariables.APP_CRASH_REPORT_ENABLED){
            Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        }
    }

    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }
}
