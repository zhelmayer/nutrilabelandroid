package hu.logicloud.nutrilabel.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import hu.logicloud.nutrilabel.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button labelSearchingBtn = (Button) findViewById(R.id.btnLabelSearching);
        labelSearchingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LabelSearchingActivity.class);
                startActivity(intent);

            }
        });

        Button showLabelStockBtn = (Button) findViewById(R.id.btnShowLabelStock);
        showLabelStockBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LabelStockActivity.class);
                startActivity(intent);
            }
        });

        Button labelingBtn = (Button) findViewById(R.id.btnLabeling);
        labelingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, StartLabelingActivity.class);
                startActivity(intent);
            }
        });

        Button stockSettingBtn = (Button) findViewById(R.id.btnStockSetting);
        stockSettingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, StockSettingActivity.class);
                startActivity(intent);
            }
        });

        Button logOutBtn = (Button) findViewById(R.id.btnLogOut);
        logOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(Intent.ACTION_MAIN);
                a.addCategory(Intent.CATEGORY_HOME);
                a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
                finish();
                System.exit(0);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
        finish();
        System.exit(0);
    }

    @Override
    public void onSuccessRead() {

    }
}
