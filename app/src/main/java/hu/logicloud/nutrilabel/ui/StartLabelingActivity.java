package hu.logicloud.nutrilabel.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import hu.logicloud.nutrilabel.R;
import hu.logicloud.nutrilabel.bll.SWEET_ALERT_LEVEL;
import hu.logicloud.nutrilabel.common.GlobalFunctions;
import hu.logicloud.nutrilabel.common.GlobalVariables;
import hu.logicloud.nutrilabel.domain.Label;
import hu.logicloud.nutrilabel.domain.Stock;
import hu.logicloud.nutrilabel.rest.ServiceResource;

public class StartLabelingActivity extends BaseActivity {

    private EditText mEanCodeEditText;
    private EditText mLabelingQty;
    private EditText mLotNumber;
    private TextView mLabelCode;
    private CheckBox mReadAll;
    private Button mStartLabelingBtn;
    private Label mLabel;
    private Stock mStock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_labeling);
        //getSupportActionBar().setTitle(R.string.start_labeling_title);

        mEanCodeEditText = (EditText) findViewById(R.id.eanCode);
        mLabelingQty = (EditText) findViewById(R.id.labelingQty);
        mLotNumber = (EditText) findViewById(R.id.lotNumber);
        mLabelCode = (TextView) findViewById(R.id.labelCode);
        mReadAll = (CheckBox) findViewById(R.id.readAll);

        mStartLabelingBtn = (Button) findViewById(R.id.startLabelingBtn);
        mStartLabelingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(anyEmptyField()){
                    showErrorMessage(getString(R.string.required));
                }else{
                    if(isValidLabelingQty()){
                        Intent intent = new Intent(StartLabelingActivity.this, LabelingActivity.class);
                        intent.putExtra("label", (Serializable) mLabel);
                        intent.putExtra("labelingQty", Integer.valueOf(mLabelingQty.getText().toString()));
                        intent.putExtra("lotNumber", mLotNumber.getText().toString());
                        intent.putExtra("readAll", mReadAll.isChecked());
                        startActivity(intent);
                    }
                }
            }

            private void showErrorMessage(String s) {
                if(TextUtils.isEmpty(mLabelingQty.getText())){mLabelingQty.setError(s);}
                if(TextUtils.isEmpty(mLotNumber.getText())){mLotNumber.setError(s);}
                if(TextUtils.isEmpty(mEanCodeEditText.getText())){mEanCodeEditText.setError(s);}
            }

            private boolean isValidLabelingQty() {
                boolean isValid = true;
                if(Integer.valueOf(mLabelingQty.getText().toString()) < 1){
                    mLabelingQty.setError(getString(R.string.min_qty));
                    isValid = false;
                }

                if(mStock == null || Integer.valueOf(mLabelingQty.getText().toString()) > Integer.valueOf(mStock.getQty())){
                    mLabelingQty.setError(getString(R.string.not_in_stock) + " " + mLabelingQty.getText()  + " " + getString(R.string.pcs));
                    isValid = false;
                }

                return isValid;
            }

            private boolean anyEmptyField() {
                return
                        TextUtils.isEmpty(mLabelingQty.getText()) ||
                        TextUtils.isEmpty(mLotNumber.getText()) ||
                        TextUtils.isEmpty(mEanCodeEditText.getText());
            }
        });
        clearData();
    }

    @Override
    public void onSuccessRead() {
        clearData();
        EditText editText = (EditText) findViewById(R.id.eanCode);
        editText.setText(getBarcodeData());
        mLabelingQty.setFocusableInTouchMode(true);
        mLabelingQty.requestFocus();

        mLabelingQty.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    mLotNumber.setFocusableInTouchMode(true);
                    mLotNumber.requestFocus();
                    return true;
                }
                return false;
            }
        });

        mLotNumber.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    mStartLabelingBtn.setFocusableInTouchMode(true);
                    mStartLabelingBtn.requestFocus();
                    return true;
                }
                return false;
            }
        });

        Boolean isEAN = GlobalFunctions.isEanCode(getBarcodeData());

        mStock = null;

        if(isEAN){
            ArrayList<List<Stock>> stock = (ArrayList<List<Stock>>) ServiceResource.getStockByEanCodeOrLabelCodeAndWarehouse(
                    GlobalVariables.getToken(),
                    isEAN.toString(),
                    getBarcodeData(),
                    GlobalVariables.getWarehouse()
            );

            if(stock != null){
                for(List<Stock> list : stock){
                    for(Stock s : list){
                        mStock = s;
                        mLabel = s.getLabel();
                        setData();
                    }
                }
            }
        }else{
            showSweetAlertdialog(this, SWEET_ALERT_LEVEL.ERROR, getString(R.string.not_ean_code), null);
            mEanCodeEditText.setText("");
        }
    }

    private void setData() {
        mEanCodeEditText.setText(mLabel.getEancode());
        mLabelCode.setText(mLabel.getLabel_code());
        mReadAll.setChecked(Boolean.valueOf(mLabel.getCheckall()));
        TextView productName = (TextView) findViewById(R.id.productName);
        productName.setText(mLabel.getDescription());
    }

    private void clearData() {
        mEanCodeEditText.setText("");
        mLabelingQty.setText("");
        mLotNumber.setText("");
        mLabelCode.setText("");
        mReadAll.setChecked(false);
        TextView productName = (TextView) findViewById(R.id.productName);
        productName.setText("");
    }

    @Override
    public void onResume() {
        super.onResume();
        clearData();
    }
}
