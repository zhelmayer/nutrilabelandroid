package hu.logicloud.nutrilabel.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import hu.logicloud.nutrilabel.R;
import hu.logicloud.nutrilabel.bll.MOVEMENT_CODE;
import hu.logicloud.nutrilabel.bll.SWEET_ALERT_LEVEL;
import hu.logicloud.nutrilabel.common.GlobalFunctions;
import hu.logicloud.nutrilabel.common.GlobalVariables;
import hu.logicloud.nutrilabel.domain.Stock;
import hu.logicloud.nutrilabel.rest.ServiceResource;

public class StockSettingActivity extends BaseActivity {

    private EditText code;
    private EditText setQty;
    private List<EditText> notFieldList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_setting);
        //getSupportActionBar().setTitle(R.string.stock_setting_title);

        code = (EditText) findViewById(R.id.code);

        setQty = (EditText) findViewById(R.id.setQty);

        Button removeFromStockBtn = (Button) findViewById(R.id.removeFromStockBtn);
        removeFromStockBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notFieldList.clear();

                if(TextUtils.isEmpty(code.getText())){
                    notFieldList.add(code);
                }

                if(TextUtils.isEmpty(setQty.getText())){
                    notFieldList.add(setQty);
                }

                if(notFieldList.isEmpty()){
                    Boolean isEAN = GlobalFunctions.isEanCode(getBarcodeData());
                    if(!isEAN){
                        setStock(code.getText().toString(), MOVEMENT_CODE.REMOVE_FROM_STOCK);
                    }
                }else{
                    for (EditText field : notFieldList){
                        field.setError(getString(R.string.required));
                    }
                }
            }
        });

        Button addToStockBtn = (Button) findViewById(R.id.addToStockBtn);
        addToStockBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notFieldList.clear();

                if(TextUtils.isEmpty(code.getText())){
                    notFieldList.add(code);
                }

                if(TextUtils.isEmpty(setQty.getText())){
                    notFieldList.add(setQty);
                }

                if(notFieldList.isEmpty()){
                    Boolean isEAN = GlobalFunctions.isEanCode(getBarcodeData());
                    if(!isEAN){
                        setStock(code.getText().toString(), MOVEMENT_CODE.ADD_TO_STOCK);
                    }
                }else{
                    for (EditText field : notFieldList){
                        field.setError(getString(R.string.required));
                    }
                }
            }
        });

    }

    private void setStock(String labelCode, MOVEMENT_CODE movementCode) {
        Boolean status = ServiceResource.setStockByLabelCodeAndLabelCodeAndWarehouse(
                GlobalVariables.getToken(),
                labelCode,
                movementCode.getValue(),
                Integer.valueOf(setQty.getText().toString()),
                "",
                GlobalVariables.getWarehouse()
        );
        if(status != null && status){
            showSweetAlertdialog(this, SWEET_ALERT_LEVEL.GOOD,
                    movementCode == MOVEMENT_CODE.REMOVE_FROM_STOCK ?
                    getString(R.string.removed) :
                    getString(R.string.added),
                    null
            );
            clearData();
            Boolean isEAN = GlobalFunctions.isEanCode(getBarcodeData());
            if(!isEAN){
                EditText editText = (EditText) findViewById(R.id.code);
                editText.setText(getBarcodeData());
                getLabelStock(isEAN);
            }
        }else{
            showSweetAlertdialog(this, SWEET_ALERT_LEVEL.ERROR, getString(R.string.error), null);
        }
    }

    @Override
    public void onSuccessRead() {
        clearData();
        Boolean isEAN = GlobalFunctions.isEanCode(getBarcodeData());
        if(!isEAN){
            EditText editText = (EditText) findViewById(R.id.code);
            editText.setText(getBarcodeData());
            getLabelStock(isEAN);
        }else{
            showSweetAlertdialog(this, SWEET_ALERT_LEVEL.ERROR, getString(R.string.not_label_code), null);
            code.setText("");
        }
    }

    private void getLabelStock(Boolean isEAN) {
        ArrayList<List<Stock>> stock = (ArrayList<List<Stock>>) ServiceResource.getStockByEanCodeOrLabelCodeAndWarehouse(
                GlobalVariables.getToken(),
                isEAN.toString(),
                getBarcodeData(),
                GlobalVariables.getWarehouse()
        );

        if(stock != null && !stock.isEmpty()){
            for(List<Stock> list : stock){
                for(Stock s : list){
                    setData(s);
                }
            }
        }else{
            BaseActivity.showSweetAlertdialog(this, SWEET_ALERT_LEVEL.ERROR, getString(R.string.label_is_not_found), null );
            code.setText("");
        }
    }

    private void clearData() {

        TextView eanCode = (TextView) findViewById(R.id.eanCode);
        eanCode.setText("");

        TextView labelCode = (TextView) findViewById(R.id.labelCode);
        labelCode.setText("");

        TextView qty = (TextView) findViewById(R.id.qty);
        qty.setText("");

        TextView warehouse = (TextView) findViewById(R.id.warehouse);
        warehouse.setText("");

        TextView productName = (TextView) findViewById(R.id.productName);
        productName.setText("");
    }

    private void setData(Stock s) {
        TextView eanCode = (TextView) findViewById(R.id.eanCode);
        eanCode.setText(s.getLabel().getEancode());

        TextView labelCode = (TextView) findViewById(R.id.labelCode);
        labelCode.setText(s.getLabel().getLabel_code());

        TextView qty = (TextView) findViewById(R.id.qty);
        qty.setText(s.getQty());

        TextView warehouse = (TextView) findViewById(R.id.warehouse);
        warehouse.setText(s.getWarehouse().getName());

        TextView productName = (TextView) findViewById(R.id.productName);
        productName.setText(s.getLabel().getDescription());
    }
}

